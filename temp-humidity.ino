#include "DHT.h"//https://github.com/markruys/arduino-DHT

#define DHT11_PIN 2

DHT dht;
 
void setup()
{
  Serial.begin(9600);
  dht.setup(DHT11_PIN);
}
 
void loop()
{
  int humidity = dht.getHumidity();
  Serial.print(humidity);
  Serial.print("%RH | ");
  
  int temperature = dht.getTemperature();
  Serial.print(temperature);
  Serial.println("*C");
 
  delay(1000);
}
